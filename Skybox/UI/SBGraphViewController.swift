//
//  SBGraphViewController.swift
//  Skybox
//
//  Created by Oleg Badretdinov on 03.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Charts
import Swinject
import RxSwift
import RangeSeekSlider

fileprivate enum GraphErrors: Error {
    case noIsinId
    case networkError
    case noData
}

fileprivate enum GraphType: String {
    case price = "Price"
    case yield = "Yield"
    
    static func allTypes() -> [GraphType] {
        return [.price, .yield]
    }
}

public class SBGraphViewController: UIViewController {
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    @IBOutlet weak var errorLabel: UILabel!
    
    public var serverUrl: String?
    public var isin: String?
    
    fileprivate var container: Container = SBGraphAssembly.getGraphContainer()
    fileprivate var networkService: SBGraphNetworkServiceProtocol!
    
    fileprivate var selectedType: GraphType = .price
    
    fileprivate var dataDisposable: Disposable?
    fileprivate var data: [SBGraphData]!
    
    override public func loadView() {
        Bundle(for: SBGraphViewController.self).loadNibNamed("SBGraphViewController", owner: self, options: nil)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.resolveDependencies()
        
        self.setupSlider()
        self.setupChart()
        self.reloadData(minDay: Int(self.rangeSlider.minValue), maxDay: Int(self.rangeSlider.maxValue))
    }
    
    fileprivate func resolveDependencies() {
        self.networkService = self.container.resolve(SBGraphNetworkServiceProtocol.self)
        self.networkService.delegate = self
    }
    
    fileprivate func setupSlider() {
        self.rangeSlider.delegate = self
    }
    
    fileprivate func setupChart() {
        self.chart.xAxis.labelPosition = .bottom
        self.chart.xAxis.axisLineColor = .black
        self.chart.xAxis.gridLineWidth = 0.3
        self.chart.xAxis.labelFont = .systemFont(ofSize: 12, weight: .semibold)
        self.chart.leftAxis.axisLineColor = .black
        self.chart.leftAxis.gridLineWidth = 0.3
        self.chart.leftAxis.labelFont = .systemFont(ofSize: 12, weight: .semibold)
        self.chart.rightAxis.axisLineColor = .black
        self.chart.rightAxis.gridLineWidth = 0.3
        self.chart.rightAxis.labelFont = .systemFont(ofSize: 12, weight: .semibold)
        
    }
    
    fileprivate func updateChartData() {
        guard let data = self.data else {
            self.showCommonError(.noData)
            return
        }
        
        let values = (0..<data.count).map { (i) -> ChartDataEntry in
            let singleData = data[i]
            let val = Double(self.selectedType == .price ? singleData.price : singleData.yield)
            return ChartDataEntry(x: Double(singleData.day), y:  val, icon: nil)
        }
        
        let set = LineChartDataSet(values: values, label: self.selectedType.rawValue)
        
        set.setColor(.red)
        set.lineWidth = 3
        set.valueFont = .systemFont(ofSize: 8, weight: .light)
        set.drawCirclesEnabled = false
        
        set.highlightEnabled = false
        
        set.mode = .cubicBezier
        
        let dataSet = LineChartData(dataSet: set)
        self.chart.data = dataSet
    }
    
    fileprivate func hideErrorIfNeeded() {
        if !self.errorLabel.isHidden {
            self.errorLabel.isHidden = true
            self.chart.isHidden = false
            self.rangeSlider.isHidden = false
        }
    }
    
    fileprivate func showCommonError(_ error: GraphErrors) {
        if self.errorLabel.isHidden {
            self.errorLabel.isHidden = false
            self.chart.isHidden = true
            self.rangeSlider.isHidden = true
        }
        
        self.errorLabel.text = error.localizedDescription
    }
    
    fileprivate func reloadData(minDay: Int, maxDay: Int) {
        guard let isin = self.isin else {
            self.showCommonError(.noIsinId)
            return
        }
        
        self.dataDisposable?.dispose()
        
        let calendar = Calendar(identifier: .gregorian)
        
        let startDateComponents = DateComponents(calendar: calendar, year: 2018, month: 12, day: minDay)
        let endDateComponents = DateComponents(calendar: calendar, year: 2018, month: 12, day: maxDay)
        
        if let startDate = calendar.date(from: startDateComponents), let endDate = calendar.date(from: endDateComponents) {
            self.dataDisposable = self.networkService.getGraphData(isin: isin, startDate: startDate, endDate: endDate).delay(1, scheduler: ConcurrentDispatchQueueScheduler(qos: .background)).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] (response) in
                guard let `self` = self else { return }
                self.data = response.data
                self.updateChartData()
            }, onError: { [weak self] (error) in
                guard let `self` = self else { return }
                self.showCommonError(.networkError)
            })
        }
    }
    
    @IBAction func handleGraphTypeButtonTap(_ sender: Any) {
        let alert = UIAlertController(title: "Type", message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        
        let priceAction = UIAlertAction(title: GraphType.price.rawValue, style: .default) { [weak self] (_) in
            guard let `self` = self else { return }
            self.selectedType = .price
            self.updateChartData()
        }
        alert.addAction(priceAction)
        
        let yieldAction = UIAlertAction(title: GraphType.yield.rawValue, style: .default) { [weak self] (_) in
            guard let `self` = self else { return }
            self.selectedType = .yield
            self.updateChartData()
        }
        alert.addAction(yieldAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension SBGraphViewController: SBGraphNetworkServiceDelegate {
    var baseUrl: String? {
        return self.serverUrl
    }
}

extension SBGraphViewController: RangeSeekSliderDelegate {
    public func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        self.reloadData(minDay: Int(minValue), maxDay: Int(maxValue))
    }
}
