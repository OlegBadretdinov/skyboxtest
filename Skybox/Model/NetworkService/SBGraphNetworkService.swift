//
//  RFGraphNetworkService.swift
//  Skybox
//
//  Created by Oleg Badretdinov on 03.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import ObjectMapper

enum NetworkErrors: Error {
    case unknown
    case urlProblem
    case connectionError
    case serverError
}

protocol SBGraphNetworkServiceDelegate: class {
    var baseUrl: String? { get }
}

protocol SBGraphNetworkServiceProtocol {
    var delegate: SBGraphNetworkServiceDelegate! { get set }
    func getGraphData(isin: String, startDate: Date, endDate: Date) -> Observable<SBGraphDataResponse>
}

class SBGraphNetworkService: NSObject {
    weak var delegate: SBGraphNetworkServiceDelegate!
    
    fileprivate func request<T: BaseMappable>(_ httpMethod :HTTPMethod,
                                              path :String) -> Observable<T> {
        guard let urlString = self.delegate?.baseUrl, let url = URL(string: urlString)?.appendingPathComponent(path) else {
            let obs = Observable<T>.create({ (observer) -> Disposable in
                observer.onError(NetworkErrors.urlProblem)
                return Disposables.create()
            })
            return obs
        }
        
        return Observable<T>.create({ (observer) -> Disposable in
            Alamofire.request(url, method: httpMethod, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                switch response.result {
                case .failure(_):
                    observer.onError(NetworkErrors.unknown)
                    return
                default:
                    break
                }
                
                if let code = response.response?.statusCode {
                    if code >= 400 && code < 500 {
                        observer.onError(NetworkErrors.connectionError)
                        return
                    } else if code >= 500 {
                        observer.onError(NetworkErrors.serverError)
                        return
                    }
                }
                
                let json = response.value as! [String : Any]
                guard let rootResponse = try? SBRootResponse<T>.init(JSON: json) else {
                    observer.onError(NetworkErrors.unknown)
                    return
                }
                
                guard rootResponse.status == SBRootResponseStatus.ok else {
                    observer.onError(NetworkErrors.serverError)
                    return
                }
                
                observer.onNext(rootResponse.response)
                observer.onCompleted()
            })
            
            return Disposables.create()
        })
    }
}

extension SBGraphNetworkService: SBGraphNetworkServiceProtocol {
    func getGraphData(isin: String, startDate: Date, endDate: Date) -> Observable<SBGraphDataResponse> {
        return self.request(.get, path: "data/\(isin)/\(startDate.timeIntervalSince1970)/\(endDate.timeIntervalSince1970)")
    }
}
