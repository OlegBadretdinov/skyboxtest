//
//  SBRootResponse.swift
//  Skybox
//
//  Created by Oleg Badretdinov on 03.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import ObjectMapper

enum SBRootResponseStatus: Int {
    case ok = 0
    case error = 1
}

class SBRootResponse<T: BaseMappable>: ImmutableMappable {
    let status :SBRootResponseStatus
    let response :T!
    
    required init(map: Map) throws {
        self.status = try map.value("status", using: EnumTransform())
        self.response = try? map.value("response")
    }
}
