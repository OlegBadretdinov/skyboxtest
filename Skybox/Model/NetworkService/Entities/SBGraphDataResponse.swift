//
//  SBGraphDataResponse.swift
//  Skybox
//
//  Created by Oleg Badretdinov on 03.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import ObjectMapper

class SBGraphDataResponse: ImmutableMappable {
    let data: [SBGraphData]
    
    required init(map: Map) throws {
        self.data = try map.value("data")
    }
}
