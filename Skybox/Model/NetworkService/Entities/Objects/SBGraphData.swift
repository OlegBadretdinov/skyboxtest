//
//  SBGraphData.swift
//  Skybox
//
//  Created by Oleg Badretdinov on 03.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import ObjectMapper

class SBGraphData: ImmutableMappable {
    let day: Int
    let price: Int
    let yield: Int
    
    required init(map: Map) throws {
        self.day = try map.value("day")
        self.price = try map.value("price")
        self.yield = try map.value("yield")
    }
}
