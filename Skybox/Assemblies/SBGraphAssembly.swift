//
//  RFGraphAssembly.swift
//  Skybox
//
//  Created by Oleg Badretdinov on 03.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Swinject

class SBGraphAssembly: NSObject {
    class func getGraphContainer() -> Container {
        let container = Container()
        container.register(SBGraphNetworkServiceProtocol.self) { _ in
            SBGraphNetworkService()
        }
        
        return container
    }
}
