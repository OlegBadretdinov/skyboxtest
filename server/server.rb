require 'sinatra'
require 'json'
require 'date'

before do
  content_type :json
end

get '/isin' do
	out = {:status => 0, :response => {
		:isin => [{
			:id => 0,
			:name => "ISIN-A"
		},{
			:id => 1,
			:name => "ISIN-B"
		},{
			:id => 2,
			:name => "ISIN-C"
		}]
	}}
	return out.to_json
end

get '/data/:isin/:startDate/:endDate' do
	startD = DateTime.strptime(params['startDate'],'%s')
	endD = DateTime.strptime(params['endDate'],'%s')
	days = endD.mjd - startD.mjd
	arr = []
	price = 100
	for i in 1..days
		delta = rand(-5..5)
		if price + delta <= 0
			delta = delta.abs
		end
		price += delta
		singleData = {
			:day => i,
			:price =>  price,
			:yield => delta
		}
		arr << singleData
	end
	return { :status => 0, :response => {
		:data => arr
	}}.to_json
end