//
//  SkyboxTests.swift
//  SkyboxTests
//
//  Created by Oleg Badretdinov on 02.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import Quick
import Nimble
import RxTest
import RxBlocking
import OHHTTPStubs
@testable import Skybox

fileprivate class Delegate: SBGraphNetworkServiceDelegate {
    var baseUrl: String? {
        get {
            return "http://example.com"
        }
    }
}

class SkyboxTests: QuickSpec {
    override func spec() {
        
        describe("NetworkService tests") {
            var network: SBGraphNetworkServiceProtocol!
            let delegate: SBGraphNetworkServiceDelegate = Delegate()
            
            beforeEach {
                network = SBGraphNetworkService()
                network.delegate = delegate
            }
            
            afterEach {
                network = nil
                OHHTTPStubs.removeAllStubs()
            }
            
            context("Correct loading", closure: {
                it("Single data", closure: {
                    stub(condition: isHost("example.com"), response: { (request) -> OHHTTPStubsResponse in
                        let path = OHPathForFile("SingleData.json", type(of: self))
                        return fixture(filePath: path!, headers: ["Content-Type":"application/json"])
                    })
                    
                    let resp = try! network.getGraphData(isin: "test", startDate: Date(), endDate: Date()).toBlocking().first()!
                    expect(resp.data.count).to(equal(1))
                    let first = resp.data.first
                    expect(first?.day).to(equal(1))
                    expect(first?.price).to(equal(99))
                    expect(first?.yield).to(equal(-1))
                })
                
                it("Multiple data", closure: {
                    stub(condition: isHost("example.com"), response: { (request) -> OHHTTPStubsResponse in
                        let path = OHPathForFile("MultipleData.json", type(of: self))
                        return fixture(filePath: path!, headers: ["Content-Type":"application/json"])
                    })
                    
                    let resp = try! network.getGraphData(isin: "test", startDate: Date(), endDate: Date()).toBlocking().first()!
                    expect(resp.data.count).to(equal(76))
                })
            })
            
            context("Error loadings", closure: {
                it("Error status in response", closure: {
                    stub(condition: isHost("example.com"), response: { (request) -> OHHTTPStubsResponse in
                        let path = OHPathForFile("ErrorData.json", type(of: self))
                        return fixture(filePath: path!, headers: ["Content-Type":"application/json"])
                    })
                    
                    var error: Error!
                    
                    let _ = network.getGraphData(isin: "test", startDate: Date(), endDate: Date()).subscribe(onNext: nil, onError: { (err) in
                        error = err
                    })
                    
                    expect(error).toEventually(matchError(NetworkErrors.serverError))
                })
                
                it("Common http error", closure: {
                    stub(condition: isHost("example.com"), response: { (request) -> OHHTTPStubsResponse in
                        let path = OHPathForFile("ErrorData.json", type(of: self))
                        return fixture(filePath: path!, status: 403, headers: ["Content-Type":"application/json"])
                    })
                    
                    var error: Error!
                    
                    let _ = network.getGraphData(isin: "test", startDate: Date(), endDate: Date()).subscribe(onNext: nil, onError: { (err) in
                        error = err
                    })
                    
                    expect(error).toEventually(matchError(NetworkErrors.connectionError))
                })
                
                it("Wrong response", closure: {
                    stub(condition: isHost("example.com"), response: { (request) -> OHHTTPStubsResponse in
                        let path = OHPathForFile("WrongResponse.json", type(of: self))
                        return fixture(filePath: path!, headers: ["Content-Type":"application/json"])
                    })
                    
                    var error: Error!
                    
                    let _ = network.getGraphData(isin: "test", startDate: Date(), endDate: Date()).subscribe(onNext: nil, onError: { (err) in
                        error = err
                    })
                    
                    expect(error).toEventually(matchError(NetworkErrors.unknown))
                })
            })
        }
    }
}
