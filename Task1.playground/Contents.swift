import Foundation
import XCTest
//
//
//Сложность 2/10
//ETA 1 час
//Фактическое - почти 50 минут
//
//сложность - линейная
//требования по памяти - минимальные. от силы несколько переменных
//

enum ArrayErrors: Error {
    case tooLarge
    case empty
    case containsLetters
}

func percentArray(_ array: inout [String]) throws { //если inout не подходит, то массив нужно создавать заранее, а не юзать append
    guard array.count <= 120000 else {
        throw ArrayErrors.tooLarge
    }
    
    let summ = array.reduce(0.0) { (result, str) -> Double in
        return result + (Double(str) ?? 0)
    }
    
    guard summ > 0 else {
        throw ArrayErrors.empty
    }
    
    let formatter: NumberFormatter = { //Можно будет вынести в корневой класс
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 3
        formatter.minimumFractionDigits = 3
        return formatter
    }()
    
    for i in 0..<array.count {
        try autoreleasepool {
            if let current = formatter.number(from: array[i])?.doubleValue, let new = formatter.string(from: NSNumber(value: current / summ * 100)) {
                array[i] = new
            } else {
                throw ArrayErrors.containsLetters
            }
        }
    }
}


//func measure(block: () -> Void) -> TimeInterval {
//    let methodStart = Date()
//    block()
//    let methodFinish = Date()
//    return methodFinish.timeIntervalSince(methodStart)
//}
//let max = 120000
//var a: [String] = Array(repeating: "", count: max)
//for i in 0..<max {
//    a[i] = "\(i)"
//}
//let time = measure { //MBP 15 2016 - 5.5s
//    try? percentArray(&a)
//}
//print(time)



//==========================================
import XCTest
class Tests: XCTestCase {
    func testZeroArray() {
        var testArray = ["0"]
        XCTAssertThrowsError(try percentArray(&testArray))
    }
    
    func testLargeArray() {
        var testArray: [String] = Array(repeating: "1", count: 500000)
        XCTAssertThrowsError(try percentArray(&testArray))
    }
    
    func testArrayWithLetters() {
        var testArray = ["asd", "1"]
        try? percentArray(&testArray)
        XCTAssertThrowsError(try percentArray(&testArray))
    }
    
    func testSingleElement() {
        var testArray = ["100"]
        try? percentArray(&testArray)
        XCTAssertEqual(testArray, ["100.000"])
    }
    
    func testNormalCase() {
        var testArray = ["2", "2"]
        try? percentArray(&testArray)
        XCTAssertEqual(testArray, ["50.000", "50.000"])
    }
}

Tests.defaultTestSuite.run()
