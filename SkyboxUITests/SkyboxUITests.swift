//
//  SkyboxUITests.swift
//  SkyboxUITests
//
//  Created by Oleg Badretdinov on 02.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import XCTest

class SkyboxUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testTypePicker() {
        let app = XCUIApplication()
        
        let priceButton = app.buttons["Price"]
        priceButton.tap()
        
        let typeSheet = app.sheets["Type"]
        typeSheet.buttons["Price"].tap()
        
        priceButton.tap()
        typeSheet.buttons["Yield"].tap()
        
        priceButton.tap()
        typeSheet.buttons["Cancel"].tap()
    }
}
